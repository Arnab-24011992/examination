Test 1: With Laravel
Main focus: To test the coding style and problem solving capabilities
Expected time: 4 hours (6 hours with the optional task)
User story:
I am a teacher and want to have a system of MCQ where I can have multiple choice questions
for my students.
1 question can have four options but will have only one right answer.
(optional - A CRUD system would be good for questions and options, otherwise entering the
values directly into the database for the sake of keeping the test small is also fine. But, in such
case, you need to provide the sql dump with all your entered data)
In the test page, a random of 5 questions (all in a single page) with their options will be shown.
The student should choose only one option per question and submit it. On the next result page,
the student should see his result - (* out of 5). Each right answer should have 1 point.
Tips:
1. Install Laravel locally (as per Laravel Installation)
2. Create the models
3. Create database migration script
4. Create CRUD functionalities (optional)
5. Create basic templates
6. Create the main test page
7. Show the result page